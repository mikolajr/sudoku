package org.rydzewski.sudoku.impl;

import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.rydzewski.sudoku.GameboardVisitorAdapter;
import org.rydzewski.sudoku.SudokuGameboard;

public class BlankFieldsGeneratorTest {

	private static final int VISIBLE_FIELDS = 10;
	private BlankFieldsGenerator generator;

	@Before
	public void setUp() throws Exception {
		generator = new BlankFieldsGenerator(new CompletedGameboardGenerator(), VISIBLE_FIELDS);
	}

	@After
	public void tearDown() throws Exception {
		generator = null;
	}

	@Test
	public final void testCreateGameboard() {
		SudokuGameboard gameboard = generator.createGameboard();
		assertFalse(gameboard.isCompleted());
		final List<Integer> blanks = new ArrayList<Integer>();
		gameboard.visit(new GameboardVisitorAdapter() {
			@Override
			public boolean visit(int col, int row, int value) {
				if (value != 0) {
					blanks.add(col);
				}
				return true;
			}
		});
		Assert.assertEquals(VISIBLE_FIELDS, blanks.size());
	}

}
