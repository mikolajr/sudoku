package org.rydzewski.sudoku.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.rydzewski.sudoku.GameboardVisitor;
import org.rydzewski.sudoku.GameboardVisitorAdapter;
import org.rydzewski.sudoku.SudokuGameboard;

public class SimpleGameboardTest {

	private SimpleGameboard gameboard;

	@Before
	public void setUp() throws Exception {
		gameboard = new SimpleGameboard();
	}

	@After
	public void tearDown() throws Exception {
		gameboard = null;
	}

	@Test
	public void testGetValueAt() {
		Assert.assertEquals(0, gameboard.getValueAt(5, 5));
		gameboard.setValueAt(5, 5, 5);
		Assert.assertEquals(5, gameboard.getValueAt(5, 5));
	}

	@Test
	public void testCopyFrom() {
		SimpleGameboard newGameboard = new SimpleGameboard();
		newGameboard.copyFrom(gameboard);
		assertTrue(newGameboard.equals(gameboard));
	}

	@Test
	public void testClone() throws CloneNotSupportedException {
		SimpleGameboard newGameboard = gameboard.clone();
		assertTrue(newGameboard.equals(gameboard));
	}

	@Test
	public void testVisitor() {
		GameboardVisitor visitor = Mockito.mock(GameboardVisitor.class);
		Mockito.when(visitor.visit(Mockito.anyInt(), Mockito.anyInt())).thenReturn(true);
		Mockito.when(visitor.visit(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(true);
		gameboard.visit(visitor);
		Mockito.verify(visitor, Mockito.times(SudokuGameboard.SIZE * SudokuGameboard.SIZE)).visit(Mockito.anyInt(),
				Mockito.anyInt(), Mockito.anyInt());
		Mockito.verify(visitor, Mockito.times(9)).visit(Mockito.anyInt(), Mockito.anyInt());
		
		visitor = Mockito.mock(GameboardVisitor.class);
		Mockito.when(visitor.visit(Mockito.anyInt(), Mockito.anyInt())).thenReturn(false);
		Mockito.when(visitor.visit(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(false);
		gameboard.visit(visitor);
		Mockito.verify(visitor, Mockito.times(1)).visit(Mockito.anyInt(), Mockito.anyInt());
		Mockito.verify(visitor, Mockito.times(0)).visit(Mockito.anyInt(),
				Mockito.anyInt(), Mockito.anyInt());

		visitor = Mockito.mock(GameboardVisitor.class);
		Mockito.when(visitor.visit(Mockito.anyInt(), Mockito.anyInt())).thenReturn(true);
		Mockito.when(visitor.visit(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(false);
		gameboard.visit(visitor);
		Mockito.verify(visitor, Mockito.times(1)).visit(Mockito.anyInt(), Mockito.anyInt());
		Mockito.verify(visitor, Mockito.times(1)).visit(Mockito.anyInt(),
				Mockito.anyInt(), Mockito.anyInt());
	}

	@Test
	public void testIsCompleted() {
		Assert.assertFalse(gameboard.isCompleted());
		gameboard.visit(new GameboardVisitorAdapter() {
			@Override
			public boolean visit(int col, int row, int value) {
				gameboard.setValueAt(col, row, 1);
				return true;
			}
		});
		assertTrue(gameboard.isCompleted());
	}

	@Test
	public final void testIsCorrect() {
		gameboard = (SimpleGameboard) new CompletedGameboardGenerator().createGameboard();
		assertTrue(gameboard.isCorrect());
		
		int oldValue = gameboard.getValueAt(1, 5);
		gameboard.setValueAt(1, 5, gameboard.getValueAt(1, 4));
		assertFalse(gameboard.isCorrect());
		gameboard.setValueAt(1, 5, oldValue);

		gameboard.setValueAt(1, 5, gameboard.getValueAt(2, 5));
		assertFalse(gameboard.isCorrect());
		gameboard.setValueAt(1, 5, oldValue);
		
		gameboard.setValueAt(1, 1, gameboard.getValueAt(2, 2));
		assertFalse(gameboard.isCorrect());
		
		gameboard = new SimpleGameboard();
		gameboard.setValueAt(1, 1, 1);
		gameboard.setValueAt(2, 2, 1);
		assertFalse(gameboard.isCorrect());

		gameboard = new SimpleGameboard();
		gameboard.setValueAt(1, 1, 1);
		gameboard.setValueAt(1, 5, 1);
		assertFalse(gameboard.isCorrect());
	}

}
