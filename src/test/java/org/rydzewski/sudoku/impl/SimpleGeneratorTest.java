package org.rydzewski.sudoku.impl;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.rydzewski.sudoku.SudokuGameboard;

public class SimpleGeneratorTest {
	
	private SimpleGenerator generator;

	@Before
	public void setUp() throws Exception {
		generator = new SimpleGenerator();
	}

	@After
	public void tearDown() throws Exception {
		generator = null;
	}

	@Test
	public final void testCreateGameboard() {
		SudokuGameboard gameboard = generator.createGameboard();
		assertTrue(gameboard.isCorrect());
	}

}
