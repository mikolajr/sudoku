package org.rydzewski.sudoku.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.rydzewski.sudoku.SudokuGameboard;

public class DefaultGameTest {
	
	private DefaultGame game;

	@Before
	public void setUp() throws Exception {
		game = new DefaultGame();
	}

	@After
	public void tearDown() throws Exception {
		game = null;
	}

	@Test
	public void testIsCompleted() {
		game.start(new CompletedGameboardGenerator().createGameboard());
		assertTrue(game.isCompleted());
		
		game.start(new SimpleGameboard());
		assertFalse(game.isCompleted());
	}

	@Test
	public void testGetGameboard() {
		game.start(new CompletedGameboardGenerator().createGameboard());
		SudokuGameboard gameboard1 = game.getGameboard();
		SudokuGameboard gameboard2 = game.getGameboard();
		assertTrue(gameboard1.equals(gameboard2));
		assertFalse(gameboard1 == gameboard2);
	}
	
	@Test
	public void testMakeMove() {
		game.start(new SimpleGameboard());
		game.makeMove(1, 1, 1);
		assertEquals(1, game.getGameboard().getValueAt(1, 1));
		game.makeMove(1, 1, 9);
		assertEquals(9, game.getGameboard().getValueAt(1, 1));
		game.makeMove(1, 2, 1);
		assertEquals(1, game.getGameboard().getValueAt(1, 2));
		
		game.start(new CompletedGameboardGenerator().createGameboard());
		assertFalse(game.makeMove(1, 1, 1));
	}

	@Test
	public void testIsMoveConflicting() {
       game.start(new SimpleGameboard());
       assertTrue(game.isMoveConflicting(1, 1, 1).isEmpty());
	}

	@Test
	public void testGetAllowedMoves() {
		game.start(new SimpleGameboard());
		boolean[] moves = game.getAllowedMoves(1, 1);
		verifyMoves(moves, "1,2,3,4,5,6,7,8,9");
	}

	private void verifyMoves(boolean[] moves, String expected) {
		boolean[] expectedMoves = new boolean[moves.length];
		String[] tokens = expected.split(",");
		for(String token : tokens) {
			int index = Integer.valueOf(token);
			expectedMoves[index-1] = true;
		}
		assertTrue(Arrays.equals(moves, expectedMoves));
	}

	@Test
	public void testUndoMove() {
		game.start(new SimpleGameboard());
		game.makeMove(1, 1, 1);
		assertEquals(1, game.getGameboard().getValueAt(1, 1));
		game.makeMove(1, 1, 9);
		assertEquals(9, game.getGameboard().getValueAt(1, 1));
		game.makeMove(1, 2, 1);
		assertEquals(1, game.getGameboard().getValueAt(1, 2));
		
		game.undoMove();
		assertEquals(0, game.getGameboard().getValueAt(1, 2));
		
		game.undoMove();
		assertEquals(1, game.getGameboard().getValueAt(1, 1));

		game.undoMove();
		assertEquals(0, game.getGameboard().getValueAt(1, 1));
	}

}
