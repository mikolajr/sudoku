/**
 * 
 */
package org.rydzewski.sudoku.impl;

import org.rydzewski.sudoku.GameboardGenerator;
import org.rydzewski.sudoku.GameboardVisitorAdapter;
import org.rydzewski.sudoku.SudokuGameboard;

/**
 * @author Mikolaj Rydzewski
 *
 */
public class CompletedGameboardGenerator implements GameboardGenerator {

	public SudokuGameboard createGameboard() {
		final SimpleGameboard gameboard = new SimpleGameboard();
		gameboard.visit(new GameboardVisitorAdapter() {
			@Override
			public boolean visit(int col, int row, int value) {
				value = (col * 3 + row + col / 3) % 9 + 1;
				gameboard.setValueAt(col, row, value);
				return true;
			}
		});
		return gameboard;
	}

}
