package org.rydzewski.sudoku;

/**
 * Interface for game generators
 * @author Mikolaj Rydzewski
 */
public interface GameboardGenerator {
	
	/**
	 * Creates correct gameboard. Number of blank fields depends on implementation 
	 * @return
	 */
	SudokuGameboard createGameboard();

}
