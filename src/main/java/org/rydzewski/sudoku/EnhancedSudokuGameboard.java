package org.rydzewski.sudoku;

import java.util.Set;

/**
 * TODO seems to be not used?
 * @author Mikolaj Rydzewski
 */
public interface EnhancedSudokuGameboard extends SudokuGameboard {

	Set<Integer> getRowValues(int row);
	
	Set<Integer> getColumnValues(int column);
	
}
