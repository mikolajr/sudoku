/**
 * 
 */
package org.rydzewski.sudoku;

import java.util.List;

import org.rydzewski.sudoku.impl.MoveMemento;

/**
 * Game controller
 * @author Mikolaj Rydzewski
 */
public interface SudokuGame {
	
	/**
	 * Starts new game with given board.
	 * @param gameboard
	 */
	void start(SudokuGameboard gameboard);
	
	/**
	 * Checks whether board is completed
	 * @return
	 */
	boolean isCompleted();
	
	/**
	 * Returns copy of current board
	 * @return
	 */
	SudokuGameboard getGameboard();
	
	/**
	 * Checks whether move conflicts with the board or not
	 * @param column
	 * @param row
	 * @param value
	 * @return 
	 */
	List<MoveConflict> isMoveConflicting(int column, int row, int value);
	
	/**
	 * Makes move if destination cell is empty.
	 * @param column
	 * @param row
	 * @param value
	 * @return
	 */
	boolean makeMove(int column, int row, int value);

	/**
	 * Returns array of allowed values in specific field. If value at n-th index is true, that such value is allowed 
	 * @param column
	 * @param row
	 * @return
	 */
	boolean[] getAllowedMoves(int column, int row);
	
	/**
	 * Reverts last move. Returns memento object that was used to restore previous state. 
	 * @return memento object or null if there were no moves yet
	 */
	MoveMemento undoMove();
	
}
