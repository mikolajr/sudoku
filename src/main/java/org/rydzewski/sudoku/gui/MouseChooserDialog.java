package org.rydzewski.sudoku.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.rydzewski.sudoku.SudokuGameboard;

@SuppressWarnings("serial")
public class MouseChooserDialog extends ValueChooserDialog {
	
	private final static Color BORDER = Color.BLACK;
	private final static Color BACKGROUND = Color.decode("#dbd9d7");
	private final static Color DISABLED_FOREROUND = Color.decode("#6b6a6a");
	private final static Color ENABLED_FOREROUND = Color.decode("#070946");
	private final static Font DISABLED_FONT = Main.FONT.deriveFont(Font.PLAIN, (float)(Main.FONT.getSize() * 0.8));
	private final static Font ENABLED_FONT = Main.FONT;

	public MouseChooserDialog(JFrame parent, Point point, boolean[] allowedMoves) {
		super(parent, point, allowedMoves);
	}

	@Override
	protected JPanel createControls() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(SudokuGameboard.SIZE / 3, SudokuGameboard.SIZE / 3));
		panel.setBackground(BACKGROUND);
		panel.setBorder(BorderFactory.createLineBorder(BORDER, 1));

		for (int i = 0; i < SudokuGameboard.SIZE; i++) {
			JPanel cell = new Cell(i + 1);
			panel.add(cell);
		}

		return panel;
	}

	private class Cell extends JPanel implements MouseListener {

		private final int cellValue;
		private boolean allowed = true;

		public Cell(int value) {
			cellValue = value;
			allowed = allowedMoves == null || allowedMoves[value - 1];
			addMouseListener(this);
			setBorder(BorderFactory.createLineBorder(BORDER, 1));
		}

		public void mouseClicked(MouseEvent e) {
			if (SwingUtilities.isRightMouseButton(e)) {
				dispose();
				return;
			}
			if (!allowed) {
				return;
			}
			value = cellValue;
			dispose();
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}

		public void mousePressed(MouseEvent e) {
		}

		public void mouseReleased(MouseEvent e) {
		}

		@Override
		protected void paintComponent(Graphics g) {
			Graphics2D g2 = (Graphics2D) g;
			g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

			Color oldColor = g.getColor();
			Font oldFont = g.getFont();
			
			g.setColor(BACKGROUND);
			g.fillRect(0, 0, getWidth(), getHeight());
			g.setColor(oldColor);

			g.setColor(allowed ? ENABLED_FOREROUND : DISABLED_FOREROUND);
			g.setFont(allowed ? ENABLED_FONT : DISABLED_FONT);

			FontMetrics fm = getFontMetrics(g.getFont());
			Rectangle2D textsize = fm.getStringBounds("" + cellValue, g);
			int xPos = (int) ((getWidth() - textsize.getWidth()) / 2);
			int yPos = (int) ((getHeight() - textsize.getHeight()) / 2 + fm.getAscent());

			g.drawString("" + cellValue, xPos, yPos);

			g.setColor(oldColor);
			g.setFont(oldFont);
		}

		@Override
		public Dimension getPreferredSize() {
			return new Dimension((int) (SudokuPanel.FIELD_SIZE * 0.75), (int) (SudokuPanel.FIELD_SIZE * 0.75));
		}
	}
}
