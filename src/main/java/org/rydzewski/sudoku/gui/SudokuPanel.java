/**
 * 
 */
package org.rydzewski.sudoku.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.rydzewski.sudoku.MoveConflict;
import org.rydzewski.sudoku.SudokuGame;
import org.rydzewski.sudoku.SudokuGameboard;
import org.rydzewski.sudoku.MoveConflict.Conflict;
import org.rydzewski.sudoku.impl.BlankFieldsGenerator;
import org.rydzewski.sudoku.impl.DefaultGame;
import org.rydzewski.sudoku.impl.MoveMemento;
import org.rydzewski.sudoku.impl.SimpleGenerator;

/**
 * @author Mikolaj Rydzewski
 */
class SudokuPanel extends JPanel implements GameHolder {

	private static final long serialVersionUID = -6865563225426419257L;

	static final int FIELD_SIZE = 40;
	final static Dimension PREFERRED_DIZE = new Dimension(FIELD_SIZE, FIELD_SIZE);

	private ClipWrapper clip;
	private Field[][] fields;
	private SudokuGame game;
	private final Container container;
	private boolean showInvalidMoves = true;
	private boolean showAllowedMoves = true;
	private boolean gameStarted;
	private Cursor cursor = new Cursor();

	public SudokuPanel(Container container) {
		super();
		this.container = container;
		clip = new ClipWrapper("write.wav");
	}

	void createControls() {
		removeAll();

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(3, 3, 4, 4));
		panel.setBackground(Color.BLACK);
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 4));

		JPanel[] mainBoxes = new JPanel[9];
		for (int i = 0; i < 9; i++) {
			JPanel boxPanel = new JPanel();
			boxPanel.setBackground(Color.GRAY);
			boxPanel.setLayout(new GridLayout(3, 3, 2, 2));
			mainBoxes[i] = boxPanel;
			panel.add(boxPanel);
		}

		SudokuGameboard gameboard = game.getGameboard();
		for (int r = 0; r < SudokuGameboard.SIZE; r++) {
			for (int c = 0; c < SudokuGameboard.SIZE; c++) {
				JComponent component;
				int value = gameboard.getValueAt(c, r);
				if (value != 0) {
					component = new Field(this, r, c, value);
				} else {
					component = new ActiveField(this, r, c, value);
				}
				fields[c][r] = (Field) component;

				int dr = (r / 3) * 3;
				int dc = (c / 3);
				mainBoxes[dr + dc].add(component);
			}
		}

		add(panel);
		container.redraw();
	}

	SudokuGame createGame(int visibleFields) {
		DefaultGame game = new DefaultGame();
		BlankFieldsGenerator gg = new BlankFieldsGenerator(new SimpleGenerator(), visibleFields);
		game.start(gg.createGameboard());
		fields = new Field[SudokuGameboard.SIZE][SudokuGameboard.SIZE];
		return game;
	}

	public Field[][] getFields() {
		return fields;
	}

	public SudokuGame getGame() {
		return game;
	}

	public void startGame(int visibleFields) {
		gameStarted = false;
		game = createGame(visibleFields);
		createControls();
	}

	public void makeMove(int column, int row, int value) {
		if (moveAllowed(column, row, value)) {
			Field field = fields[column][row];
			if (field.getValue() == value) {
				value = 0;
			}
			if (game.makeMove(column, row, value)) {
				gameStarted = true;
				clip.play(1);
				field.setFieldValue(value);
				field.repaint();

				if (game.isCompleted()) {
					JOptionPane.showMessageDialog(SwingUtilities.windowForComponent(this), "juhuuuu!");
				}
			}
		}
	}

	private boolean moveAllowed(int column, int row, int value) {
		if (!showInvalidMoves || value == 0) {
			return true;
		}
		List<MoveConflict> conflict = game.isMoveConflicting(column, row, value);
		if (conflict.isEmpty()) {
			return true;
		}

		for (MoveConflict moveConflict : conflict) {
			highlightInvalid(moveConflict);
		}

		return false;
	}

	private void highlightInvalid(MoveConflict conflict) {
		fields[conflict.getColumn()][conflict.getRow()].setError(true);
		for (int i = 0; i < SudokuGameboard.SIZE; i++) {
			if (conflict.getConflict() == Conflict.COLUMN) {
				fields[conflict.getColumn()][i].setError(true);
			} else if (conflict.getConflict() == Conflict.ROW) {
				fields[i][conflict.getRow()].setError(true);
			}
		}
	}

	public boolean isShowInvalidMoves() {
		return showInvalidMoves;
	}

	public void setShowInvalidMoves(boolean showInvalidMoves) {
		this.showInvalidMoves = showInvalidMoves;
	}

	public int chooseValue(Point point, Field source) {
		JFrame frame = container.getFrame();
		point = SwingUtilities.convertPoint(source, point, frame);
		point.translate(frame.getLocation().x, frame.getLocation().y);

		boolean[] allowed = null;
		if (showAllowedMoves) {
			allowed = game.getAllowedMoves(source.getColumn(), source.getRow());
		}

		MouseChooserDialog flg = new MouseChooserDialog(frame, point, allowed);
		return flg.getValue();
	}

	public boolean isShowAllowedMoves() {
		return showAllowedMoves;
	}

	public void setShowAllowedMoves(boolean showAllowedMoves) {
		this.showAllowedMoves = showAllowedMoves;
	}

	public void undoMove() {
		MoveMemento memento = game.undoMove();
		if (memento != null) {
			Field field = fields[memento.getColumn()][memento.getRow()];
			field.setFieldValue(memento.getValue());
			field.repaint();
		}
	}

	public boolean isGameStarted() {
		return gameStarted;
	}

	public void moveCursorTo(int column, int row) {
		cursor.moveCursorTo(column, row);
		repaintGameboard();
	}

	private void repaintGameboard() {
		Position position = cursor.getPosition();
		Map<Field, FieldState> cache = new HashMap<Field, FieldState>();

		for (int r = 0; r < 9; r++) {
			for (int c = 0; c < 9; c++) {
				cache.put(fields[c][r], FieldState.NONE);
			}
		}

		for (int i = 0; i < 9; i++) {
			cache.put(fields[position.column][i], FieldState.SELECTED_ROW);
			cache.put(fields[i][position.row], FieldState.SELECTED_ROW);
		}
		int sc = (position.column / 3) * 3;
		int sr = (position.row / 3) * 3;
		for (int i = 0; i < 9; i++) {
			int dr = i / 3;
			int dc = i % 3;
			cache.put(fields[sc + dc][sr + dr], FieldState.SELECTED_SQUARE);
		}

		cache.put(fields[position.column][position.row], FieldState.HOVER);

		for (Entry<Field, FieldState> element : cache.entrySet()) {
			element.getKey().setState(element.getValue());
		}
	}

	private void clearErrorFields() {
		Map<Field, Boolean> cache = new HashMap<Field, Boolean>();

		for (int r = 0; r < 9; r++) {
			for (int c = 0; c < 9; c++) {
				cache.put(fields[c][r], Boolean.FALSE);
			}
		}

		for (Entry<Field, Boolean> element : cache.entrySet()) {
			element.getKey().setError(element.getValue());
		}
	}

	public void resetMove() {
		clearErrorFields();
		repaintGameboard();
	}

	public void moveCursorBy(int columns, int rows) {
		clearErrorFields();
		cursor.moveCursorBy(columns, rows);
		repaintGameboard();
	}

	private static class Cursor {
		private int column;
		private int row;

		synchronized void moveCursorTo(int column, int row) {
			this.column = column;
			this.row = row;
			validate();
		}

		synchronized void moveCursorBy(int columns, int rows) {
			this.column += columns;
			this.row += rows;
			validate();
		}

		void validate() {
			if (column < 0) {
				column = SudokuGameboard.SIZE - 1;
			}
			if (column >= SudokuGameboard.SIZE) {
				column = 0;
			}
			if (row < 0) {
				row = SudokuGameboard.SIZE - 1;
			}
			if (row >= SudokuGameboard.SIZE) {
				row = 0;
			}
		}
		synchronized Position getPosition() {
			return new Position(column, row);
		}
	}
	private static class Position {
		final int column;
		final int row;
		public Position(int column, int row) {
			this.column = column;
			this.row = row;
		}
	}
	public void makeMove(int value) {
		Position position = cursor.getPosition();
		makeMove(position.column, position.row, value);
	}
}
