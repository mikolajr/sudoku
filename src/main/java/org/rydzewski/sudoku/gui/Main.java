package org.rydzewski.sudoku.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import org.simplericity.macify.eawt.Application;
import org.simplericity.macify.eawt.ApplicationEvent;
import org.simplericity.macify.eawt.ApplicationListener;
import org.simplericity.macify.eawt.DefaultApplication;

/**
 * Main Swing window
 * 
 * @author Mikolaj Rydzewski
 */
@SuppressWarnings("serial")
public class Main extends JFrame implements Container, ApplicationListener {

	private static final String TITLE = Messages.getString("application.title");
	private static final int VISIBLE_FIELDS_HARDCORE = 20;
	private static final int VISIBLE_FIELDS_DIFFICULT = 40;
	private static final int VISIBLE_FIELDS_MEDIUM = 50;
	private static final int VISIBLE_FIELDS_EASY = 70;
	final static Color SELECTED_SQUARE = Color.decode("#db8181");
	final static Color SELECTED_ROW = Color.decode("#88a1d9");
	final static Color HOVER = Color.decode("#db4848");
	final static Color FONT_COLOR = Color.decode("#070946");
	final static Color FONT_ACTIVE_COLOR = Color.decode("#0a5b10");
	final static Font FONT = new Font("SansSerif", Font.BOLD, SudokuPanel.FIELD_SIZE / 2);

	private SudokuPanel sudokuPanel;
	private StartAction startAction;

	private int startGameWithVisibleFields = VISIBLE_FIELDS_MEDIUM;
	private final Application application;
	private Action aboutAction;

	public Main(Application application) {
		super(TITLE);
		this.application = application;
		setDefaultCloseOperation(application.isMac() ? HIDE_ON_CLOSE : DISPOSE_ON_CLOSE);
		setIconImage(loadIcon("sudoku.png"));
		setResizable(false);
		sudokuPanel = new SudokuPanel(this);
		addKeyListener(new KeyboardController(sudokuPanel));
		createMenu();
		getContentPane().add(sudokuPanel);
		sudokuPanel.startGame(startGameWithVisibleFields);
		setLocationRelativeTo(null);
	}

	private Image loadIcon(String icon) {
		try {
			return ImageIO.read(getClass().getClassLoader().getResourceAsStream(icon));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private void createMenu() {
		JMenuBar menuBar = new JMenuBar();

		JMenu menu = new JMenu(Messages.getString("menu.menu_name"));
		menuBar.add(menu);

		startAction = new StartAction(Messages.getString("menu.new_game"));
		menu.add(new JMenuItem(startAction));

		menu.add(new UndoMoveAction(Messages.getString("menu.undo_last_move")));

		menu.add(new JSeparator());

		ButtonGroup group = new ButtonGroup();
		JMenuItem menuItem = new JRadioButtonMenuItem();
		Action action = new SelectDifficultyAction(Messages.getString("menu.easy"), VISIBLE_FIELDS_EASY, menuItem);
		menuItem.setAction(action);
		menu.add(menuItem);
		group.add(menuItem);

		menuItem = new JRadioButtonMenuItem();
		action = new SelectDifficultyAction(Messages.getString("menu.medium"), VISIBLE_FIELDS_MEDIUM, menuItem);
		menuItem.setAction(action);
		menu.add(menuItem);
		group.add(menuItem);
		action.actionPerformed(null);

		menuItem = new JRadioButtonMenuItem();
		action = new SelectDifficultyAction(Messages.getString("menu.difficult"), VISIBLE_FIELDS_DIFFICULT, menuItem);
		menuItem.setAction(action);
		menu.add(menuItem);
		group.add(menuItem);

		menuItem = new JRadioButtonMenuItem();
		action = new SelectDifficultyAction(Messages.getString("menu.hardcore"), VISIBLE_FIELDS_HARDCORE, menuItem);
		menuItem.setAction(action);
		menu.add(menuItem);
		group.add(menuItem);

		menu.add(new JSeparator());

		menuItem = new JCheckBoxMenuItem();
		action = new ShowInvalidMovesAction(Messages.getString("menu.show_invalid_moves"), menuItem);
		menuItem.setAction(action);
		menu.add(menuItem);

		menuItem = new JCheckBoxMenuItem();
		action = new ShowAllowedMovesAction(Messages.getString("menu.show_allowed_moves"), menuItem);
		menuItem.setAction(action);
		menu.add(menuItem);

		if (!application.isMac()) {
			menu.add(new JSeparator());
			menu.add(new JMenuItem(new ExitAction()));
		}

		aboutAction = new AboutAction(Messages.getString("menu.about"));
		if (!application.isMac()) {
			menu = new JMenu(Messages.getString("menu.help"));
			menuBar.add(menu);

			menuItem = new JMenuItem(aboutAction);
			menu.add(menuItem);
		}

		setJMenuBar(menuBar);
	}

	private class AboutAction extends AbstractAction {

		public AboutAction(String title) {
			super(title);
		}

		public void actionPerformed(ActionEvent e) {
			new AboutDialog(Main.this).setVisible(true);
		}
	}

	private class ExitAction extends AbstractAction {

		public ExitAction() {
			super(Messages.getString("menu.exit"));
			putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_DOWN_MASK));
		}

		public void actionPerformed(ActionEvent e) {
			dispose();
		}
	}

	private class StartAction extends AbstractAction {

		StartAction(String title) {
			super(title);
			putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
		}

		public void actionPerformed(ActionEvent e) {
			if (sudokuPanel.isGameStarted()) {
				int answer = JOptionPane.showConfirmDialog(Main.this, Messages.getString("question.start_new_game"),
						TITLE, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (answer != JOptionPane.YES_OPTION) {
					return;
				}
			}
			sudokuPanel.startGame(startGameWithVisibleFields);
		}
	}

	private class SelectDifficultyAction extends AbstractAction {

		private final int visibleFields;
		private final JMenuItem menuItem;

		SelectDifficultyAction(String title, int visibleFields, JMenuItem menuItem) {
			super(title);
			this.visibleFields = visibleFields;
			this.menuItem = menuItem;
		}

		public void actionPerformed(ActionEvent e) {
			startGameWithVisibleFields = this.visibleFields;
			menuItem.setSelected(true);
			startAction.actionPerformed(e);
		}
	}

	private class ShowInvalidMovesAction extends AbstractAction {

		private final JMenuItem menuItem;

		public ShowInvalidMovesAction(String title, JMenuItem menuItem) {
			super(title);
			this.menuItem = menuItem;
			menuItem.setSelected(sudokuPanel.isShowInvalidMoves());
		}

		public void actionPerformed(ActionEvent e) {
			sudokuPanel.setShowInvalidMoves(!sudokuPanel.isShowInvalidMoves());
			menuItem.setSelected(sudokuPanel.isShowInvalidMoves());
		}
	}

	private class ShowAllowedMovesAction extends AbstractAction {

		private final JMenuItem menuItem;

		public ShowAllowedMovesAction(String title, JMenuItem menuItem) {
			super(title);
			this.menuItem = menuItem;
			menuItem.setSelected(sudokuPanel.isShowAllowedMoves());
		}

		public void actionPerformed(ActionEvent e) {
			sudokuPanel.setShowAllowedMoves(!sudokuPanel.isShowAllowedMoves());
			menuItem.setSelected(sudokuPanel.isShowAllowedMoves());
		}
	}

	private class UndoMoveAction extends AbstractAction {

		public UndoMoveAction(String title) {
			super(title);
			putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK));
		}

		public void actionPerformed(ActionEvent e) {
			sudokuPanel.undoMove();
		}
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Application application = new DefaultApplication();
				Main main = new Main(application);
				application.addApplicationListener(main);
				application.addAboutMenuItem();
				main.setVisible(true);
			}
		});
	}

	public void redraw() {
		pack();
	}

	public JFrame getFrame() {
		return this;
	}

	public void handleAbout(ApplicationEvent e) {
		aboutAction.actionPerformed(null);
		e.setHandled(true);
	}

	public void handleOpenApplication(ApplicationEvent e) {
	}

	public void handleOpenFile(ApplicationEvent e) {
	}

	public void handlePreferences(ApplicationEvent e) {
	}

	public void handlePrintFile(ApplicationEvent e) {
	}

	public void handleQuit(ApplicationEvent e) {
		System.exit(0);
	}

	public void handleReOpenApplication(ApplicationEvent e) {
		setVisible(true);
		e.setHandled(true);
	}
}
