/**
 * 
 */
package org.rydzewski.sudoku.gui;

import java.awt.Point;

import org.rydzewski.sudoku.SudokuGame;

/**
 * @author Mikolaj Rydzewski
 */
interface GameHolder {
	Field[][] getFields();
	SudokuGame getGame();
	void startGame(int visibleFields);
	void makeMove(int column, int row, int value);
	int chooseValue(Point point, Field source);
	void undoMove();
	void moveCursorTo(int column, int row);
	void resetMove();
	void moveCursorBy(int columns, int rows);
	void makeMove(int value);
}
