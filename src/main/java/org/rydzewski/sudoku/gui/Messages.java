package org.rydzewski.sudoku.gui;

import java.util.ResourceBundle;

public class Messages {
   private static ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("messages");

   private Messages() {
   }

   public static String getString(String key) {
      return RESOURCE_BUNDLE.getString(key);
   }
}
