/**
 * 
 */
package org.rydzewski.sudoku.gui;

enum FieldState {
	NONE, SELECTED_ROW, SELECTED_SQUARE, HOVER
}