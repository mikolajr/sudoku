/**
 * 
 */
package org.rydzewski.sudoku.gui;

import javax.swing.JFrame;


/**
 * @author Mikolaj Rydzewski
 *
 */
public interface Container {
	
	void redraw();
	JFrame getFrame();
}
