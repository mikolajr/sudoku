/**
 * 
 */
package org.rydzewski.sudoku.gui;

import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JDialog;

/**
 * @author Mikolaj Rydzewski
 */
@SuppressWarnings("serial")
public class AboutDialog extends JDialog {

	public AboutDialog(Frame owner) throws HeadlessException {
		super(owner);
		setUndecorated(true);
		ImagePane imagePanel = new ImagePane("splash.png");
		imagePanel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dispose();
			}
		});
		getContentPane().add(imagePanel);
		pack();
		setLocationRelativeTo(getParent());
		setModal(true);
	}
}
