/**
 * 
 */
package org.rydzewski.sudoku.gui;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * @author Mikolaj Rydzewski
 */
public class KeyboardController extends KeyAdapter {

	private final GameHolder gameHolder;

	public KeyboardController(GameHolder gameHolder) {
		this.gameHolder = gameHolder;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_LEFT:
		case KeyEvent.VK_KP_LEFT:
			gameHolder.moveCursorBy(-1, 0);
			break;
		case KeyEvent.VK_RIGHT:
		case KeyEvent.VK_KP_RIGHT:
			gameHolder.moveCursorBy(1, 0);
			break;
		case KeyEvent.VK_UP:
		case KeyEvent.VK_KP_UP:
			gameHolder.moveCursorBy(0, -1);
			break;
		case KeyEvent.VK_DOWN:
		case KeyEvent.VK_KP_DOWN:
			gameHolder.moveCursorBy(0, 1);
			break;
		case KeyEvent.VK_0:
		case KeyEvent.VK_1:
		case KeyEvent.VK_2:
		case KeyEvent.VK_3:
		case KeyEvent.VK_4:
		case KeyEvent.VK_5:
		case KeyEvent.VK_6:
		case KeyEvent.VK_7:
		case KeyEvent.VK_8:
		case KeyEvent.VK_9:
		case KeyEvent.VK_NUMPAD0:
		case KeyEvent.VK_NUMPAD1:
		case KeyEvent.VK_NUMPAD2:
		case KeyEvent.VK_NUMPAD3:
		case KeyEvent.VK_NUMPAD4:
		case KeyEvent.VK_NUMPAD5:
		case KeyEvent.VK_NUMPAD6:
		case KeyEvent.VK_NUMPAD7:
		case KeyEvent.VK_NUMPAD8:
		case KeyEvent.VK_NUMPAD9:
			int value = e.getKeyChar() - '0';
			gameHolder.makeMove(value);
			break;
		case KeyEvent.VK_SPACE:
			gameHolder.makeMove(0);
			break;
		default:
		}
	}
}
