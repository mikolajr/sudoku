/**
 * 
 */
package org.rydzewski.sudoku.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

@SuppressWarnings("serial")
class Field extends JPanel implements MouseListener {

	protected final GameHolder gameHolder;
	protected int row;
	protected int column;
	private int value;
	private FieldState state = FieldState.NONE;
	private boolean error;

	public Field(GameHolder gameHolder, int row, int column, int value) {
		super();
		this.gameHolder = gameHolder;
		this.row = row;
		this.column = column;
		this.value = value;
		addMouseListener(this);
	}

	@Override
	public Dimension getPreferredSize() {
		return SudokuPanel.PREFERRED_DIZE;
	}

	@Override
	protected void paintComponent(Graphics g) {
		Color oldColor = g.getColor();
		Font oldFont = g.getFont();

		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

		g.setColor(getBackgroundColor());
		g.fillRect(0, 0, getWidth(), getHeight());

		g.setColor(getForegroundColor());
		g.setFont(Main.FONT);

		FontMetrics fm = getFontMetrics(g.getFont());
		Rectangle2D textsize = fm.getStringBounds(getFieldValue(), g);
		int xPos = (int) ((getWidth() - textsize.getWidth()) / 2);
		int yPos = (int) ((getHeight() - textsize.getHeight()) / 2 + fm.getAscent());

		g.drawString(getFieldValue(), xPos, yPos);

		g.setColor(oldColor);
		g.setFont(oldFont);
	}

	protected String getFieldValue() {
		return value == 0 ? "" : String.valueOf(value);
	}

	protected Color getForegroundColor() {
		return Main.FONT_COLOR;
	}

	protected Color getBackgroundColor() {
		Color c = null;
		switch (state) {
		case NONE:
			c = Color.LIGHT_GRAY;
			break;
		case HOVER:
			c = Main.HOVER;
			break;
		case SELECTED_ROW:
			c = Main.SELECTED_ROW;
			break;
		case SELECTED_SQUARE:
			c = Main.SELECTED_SQUARE;
		}
		if (isError()) {
			c = Color.yellow;
		}
		return c;
	}

	public void mouseClicked(MouseEvent e) {
		gameHolder.resetMove();
	}

	public FieldState getState() {
		return state;
	}

	public void setState(FieldState state) {
		FieldState old = this.state;
		this.state = state;
		if (old != state) {
			repaint();
		}
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
		gameHolder.moveCursorTo(column, row);
	}

	public void mouseExited(MouseEvent e) {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Field other = (Field) obj;
		if (column != other.column)
			return false;
		if (row != other.row)
			return false;
		return true;
	}

	public void setFieldValue(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		boolean oldError = this.error;
		this.error = error;
		if (oldError != error) {
			repaint();
		}
	}

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}
}