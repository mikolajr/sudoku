/**
 * 
 */
package org.rydzewski.sudoku.gui;

import java.awt.Color;
import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
class ActiveField extends Field {

	public ActiveField(GameHolder gameHolder, int row, int column, int value) {
		super(gameHolder, row, column, value);
	}

	public void mouseClicked(MouseEvent e) {
		gameHolder.resetMove();
		if (!SwingUtilities.isLeftMouseButton(e)) {
			return;
		}
		int newValue = 0;
		if (!e.isControlDown()) {
			newValue = gameHolder.chooseValue(e.getPoint(), this);
			if (newValue == 0) {
				return;
			}
		}
		gameHolder.makeMove(column, row, newValue);
	}

	protected Color getForegroundColor() {
		return Main.FONT_ACTIVE_COLOR;
	}
}