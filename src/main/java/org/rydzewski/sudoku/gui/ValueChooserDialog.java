/**
 * 
 */
package org.rydzewski.sudoku.gui;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author Mikolaj Rydzewski
 */
@SuppressWarnings("serial")
public class ValueChooserDialog extends JDialog {

	protected int value;
	protected final boolean[] allowedMoves;
	
	public ValueChooserDialog(JFrame parent, Point point, boolean[] allowedMoves) {
		super(parent);
		this.allowedMoves = allowedMoves;
		setUndecorated(true);
		getContentPane().add(createControls());
		pack();
		setLocation(point.x - getWidth()/2, point.y - getHeight()/2);
		setModal(true);
		setVisible(true);
	}

	protected JPanel createControls() {
		JPanel panel = new JPanel();
		
		final JTextField e = new JTextField(5);
		e.setHorizontalAlignment(JTextField.CENTER);
		e.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				value = Integer.valueOf(e.getText());
				dispose();
			}
		});
		e.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					dispose();
				}
			}
		});
		panel.add(e);
		
		return panel;
	}

	public int getValue() {
		return value;
	}
}
