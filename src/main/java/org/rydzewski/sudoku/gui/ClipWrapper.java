/**
 * 
 */
package org.rydzewski.sudoku.gui;

import java.io.Closeable;
import java.io.IOException;
import java.net.URL;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 * @author Mikolaj Rydzewski
 * 
 */
public class ClipWrapper implements Closeable {

	private Clip clip;

	public ClipWrapper(String name) {
		try {
			URL url = getClass().getClassLoader().getResource(name);
			AudioInputStream audioInputStream;
			audioInputStream = AudioSystem.getAudioInputStream(url);
			clip = AudioSystem.getClip();
			clip.open(audioInputStream);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void play(final int repeatCount) {
		if (clip != null) {
			clip.start();
			clip.loop(repeatCount);
		}
	}

	@Override
	protected void finalize() throws Throwable {
		close();
		super.finalize();
	}

	public synchronized void close() throws IOException {
		if (clip != null) {
			clip.stop();
			clip.close();
			clip = null;
		}
	}
}
