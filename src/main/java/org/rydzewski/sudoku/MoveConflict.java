/**
 * 
 */
package org.rydzewski.sudoku;

/**
 * Description of conflicting move
 * @author Mikolaj Rydzewski
 */
public class MoveConflict {
	
	public enum Conflict {
		CELL,
		ROW,
		COLUMN
	}

	/**
	 * type of conflict
	 */
	private final Conflict conflict;
	/**
	 * column of conflicting cell
	 */
	private final int column;
	/**
	 * row of conflicting cell
	 */
	private final int row;
	/**
	 * conflicting value
	 */
	private final int value;
	
	public MoveConflict(Conflict conflict, int column, int row, int value) {
		super();
		this.conflict = conflict;
		this.column = column;
		this.row = row;
		this.value = value;
	}
	public Conflict getConflict() {
		return conflict;
	}
	public int getColumn() {
		return column;
	}
	public int getRow() {
		return row;
	}
	public int getValue() {
		return value;
	}
}
