package org.rydzewski.sudoku;

/**
 * @author Mikolaj Rydzewski
 * @param <T>
 */
public interface Copiable<T> {
	
	T copyFrom(T source);

}
