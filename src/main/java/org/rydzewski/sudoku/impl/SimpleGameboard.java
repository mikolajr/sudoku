package org.rydzewski.sudoku.impl;

import java.util.Arrays;

import org.rydzewski.sudoku.Copiable;
import org.rydzewski.sudoku.SudokuGameboard;
import org.rydzewski.sudoku.WritableSudokuGameboard;

/**
 * @author Mikolaj Rydzewski
 */
public class SimpleGameboard extends AbstractSudokuGameboard implements
		Copiable<SudokuGameboard>, WritableSudokuGameboard, Cloneable {

	private int[][] board = new int[SIZE][SIZE];

	public int getValueAt(int column, int row) {
		Validate.isTrue(column >= 0 && column < SIZE, "Invalid column " + column);
		Validate.isTrue(row >= 0 && row < SIZE, "Invalid row " + row);

		return board[column][row];
	}

	public SimpleGameboard copyFrom(SudokuGameboard source) {
		for (int r = 0; r < SIZE; r++) {
			for (int c = 0; c < SIZE; c++) {
				setValueAt(c, r, source.getValueAt(c, r));
			}
		}
		return this;
	}

	public void setValueAt(int column, int row, int value) {
		Validate.isTrue(value >=0 && value <= SudokuGameboard.SIZE, "Illegal value: " + value);
		Validate.isTrue(column >= 0 && column < SIZE, "Invalid column " + column);
		Validate.isTrue(row >= 0 && row < SIZE, "Invalid row " + row);
		
		board[column][row] = value;
	}

	@Override
	public SimpleGameboard clone() throws CloneNotSupportedException {
		return (SimpleGameboard) super.clone();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(board);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleGameboard other = (SimpleGameboard) obj;
		if (!Arrays.deepEquals(board, other.board))
			return false;
		return true;
	}

}
