package org.rydzewski.sudoku.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.rydzewski.sudoku.MoveConflict;
import org.rydzewski.sudoku.SudokuGame;
import org.rydzewski.sudoku.SudokuGameboard;
import org.rydzewski.sudoku.MoveConflict.Conflict;

/**
 * @author Mikolaj Rydzewski
 */
public class DefaultGame implements SudokuGame {

	private SimpleGameboard initialBoard;
	private SimpleGameboard board;
	private Stack<MoveMemento> moves = new Stack<MoveMemento>();

	public void start(SudokuGameboard gameboard) {
		initialBoard = new SimpleGameboard().copyFrom(gameboard);
		board = new SimpleGameboard().copyFrom(gameboard);
		moves.clear();
	}

	public boolean isCompleted() {
		return board.isCorrect() && board.isCompleted();
	}

	public SudokuGameboard getGameboard() {
		try {
			return (SudokuGameboard) board.clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean makeMove(int column, int row, int value) {
		if (initialBoard.getValueAt(column, row) != 0)
			return false;
		
		MoveMemento memento = new MoveMemento(column, row, board.getValueAt(column, row));
		moves.add(memento);
		
		board.setValueAt(column, row, value);
		return true;
	}

	public List<MoveConflict> isMoveConflicting(int column, int row, int value) {
		List<MoveConflict> conflicts = new ArrayList<MoveConflict>();
		
		int sc = (column / 3) * 3;
		int sr = (row / 3) * 3;
		for(int i=0; i<SudokuGameboard.SIZE; i++) {
			if (i != row && board.getValueAt(column, i) == value) {
				conflicts.add(new MoveConflict(Conflict.COLUMN, column, i, value));
			}
			
			if (i != column && board.getValueAt(i, row) == value) {
				conflicts.add(new MoveConflict(Conflict.ROW, i, row, value));
			}
			
			int dr = i / 3;
			int dc = i % 3;
			int currentRow = sr + dr;
			int currentColumn = sc + dc;
			if (currentColumn != column && currentRow != row && board.getValueAt(currentColumn, currentRow) == value) {
				conflicts.add(new MoveConflict(Conflict.CELL, currentColumn, currentRow, value));
			}
		}
		
		return conflicts;
	}

	public boolean[] getAllowedMoves(int column, int row) {
		boolean[] result = new boolean[SudokuGameboard.SIZE];

		for(int i=0; i<SudokuGameboard.SIZE; i++) {
			if (isMoveConflicting(column, row, i+1).isEmpty()) {
				result[i] = true;
			}
		}
		
		return result;
	}

	public MoveMemento undoMove() {
       MoveMemento memento = null;
		if (!moves.isEmpty()) {
		    memento = moves.pop();
			board.setValueAt(memento.getColumn(), memento.getRow(), memento.getValue());
		}
		return memento;
	}
}
