package org.rydzewski.sudoku.impl;

import java.util.Set;
import java.util.TreeSet;

import org.rydzewski.sudoku.GameboardGenerator;
import org.rydzewski.sudoku.SudokuGameboard;

/**
 * Generates random set of blank fields on gameboard created by delegate
 * generator
 * 
 * @author Mikolaj Rydzewski
 */
public class BlankFieldsGenerator implements GameboardGenerator {

	private GameboardGenerator gameboardGenerator;
	private int visibleFields;

	/**
	 * @param gameboardGenerator
	 *            delegate used to generate complete random board
	 * @param visibleFields
	 *            number of non blank fields
	 */
	public BlankFieldsGenerator(GameboardGenerator gameboardGenerator, int visibleFields) {
		super();
		this.gameboardGenerator = gameboardGenerator;
		if (visibleFields > SudokuGameboard.SIZE * SudokuGameboard.SIZE)
			throw new IllegalArgumentException("visibleFields value too large " + visibleFields);
		this.visibleFields = visibleFields;
	}

	public SudokuGameboard createGameboard() {
		SudokuGameboard gameboard = gameboardGenerator.createGameboard();
		return clearFields(gameboard);
	}

	private SudokuGameboard clearFields(SudokuGameboard source) {
		SimpleGameboard board = new SimpleGameboard().copyFrom(source);

		Set<Integer> fieldsToClear = generateRandomFields(visibleFields);
		for (Integer field : fieldsToClear) {
			int row = field / SimpleGameboard.SIZE;
			int column = field % SimpleGameboard.SIZE;
			board.setValueAt(column, row, 0);
		}

		return board;
	}

	private Set<Integer> generateRandomFields(int visibleFields) {
		Set<Integer> fieldsToLeft = new TreeSet<Integer>();
		int maxSize = SimpleGameboard.SIZE * SimpleGameboard.SIZE;

		while (fieldsToLeft.size() < visibleFields) {
			Integer value = RandomUtils.nextInt(maxSize);
			fieldsToLeft.add(value);
		}

		Set<Integer> result = new TreeSet<Integer>();
		for (int i = 0; i < maxSize; i++) {
			if (!fieldsToLeft.contains(Integer.valueOf(i))) {
				result.add(i);
			}
		}

		return result;
	}

}
