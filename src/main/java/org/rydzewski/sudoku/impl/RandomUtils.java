package org.rydzewski.sudoku.impl;

import java.util.Random;

public class RandomUtils {

	private static final Random RANDOM = new Random();
	
	public static Integer nextInt(int maxSize) {
		return RANDOM.nextInt(maxSize);
	}

}
