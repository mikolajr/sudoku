/**
 * 
 */
package org.rydzewski.sudoku.impl;

/**
 * Memento object for move undo operation. Saves previous values at specified location.
 * @author Mikolaj Rydzewski
 */
public class MoveMemento {
	
	private final int column;
	private final int row;
	private final int value;

	public MoveMemento(int column, int row, int value) {
		this.column = column;
		this.row = row;
		this.value = value;
	}

	public int getColumn() {
		return column;
	}

	public int getRow() {
		return row;
	}

	public int getValue() {
		return value;
	}
}
