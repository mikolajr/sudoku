package org.rydzewski.sudoku.impl;

import org.rydzewski.sudoku.GameboardVisitor;
import org.rydzewski.sudoku.SudokuGameboard;

/**
 * @author Mikolaj Rydzewski
 */
public abstract class AbstractSudokuGameboard implements SudokuGameboard {

	public boolean isCompleted() {
		for(int r=0; r<SIZE; r++) {
			for(int c=0; c<SIZE; c++) {
				if (getValueAt(c, r) == 0) return false;
			}
		}
		return true;
	}

	public boolean isCorrect() {
		if (!checkRows())
			return false;
		
		if (!checkColumns())
			return false;

		for(int r=0; r<SIZE/3;  r++) {
			for(int c=0; c<SIZE/3; c++) {
				if (!checkSquare(c*3, r*3))
					return false;
			}
		}
		
		return true;
	}

	/**
	 * Checks whether pointed square contains correct set of values (e.g. no duplicates) 
	 * @param startColumn
	 * @param startRow
	 * @return
	 */
	private boolean checkSquare(int startColumn, int startRow) {
		boolean[] buffer = new boolean[SIZE];
		
		for(int r=0; r<SIZE/3; r++) {
			for(int c=0; c<SIZE/3; c++) {
				int value = getValueAt(startColumn + c, startRow + r);
				if (value == 0) {
					continue;
				}
				if (buffer[value-1]) {
					return false;
				}
				buffer[value-1] = true;
			}
		}
		
		return true;
	}

	/**
	 * Checks whether columns contain correct set of values (e.g. no duplicates)
	 * @return
	 */
	private boolean checkColumns() {
		for(int column=0; column<SIZE; column++) {
			boolean[] buffer = new boolean[SIZE];
			
			for(int row=0; row<SIZE; row++) {
				int value = getValueAt(column, row);
				if (value == 0) {
					continue;
				}
				if (buffer[value-1]) {
					return false;
				}
				buffer[value-1] = true;
			}
		}
		
		return true;
	}

	/**
	 * Checks whether rows contain correct set of values (e.g. no duplicates)
	 * @return
	 */
	private boolean checkRows() {
		for(int row=0; row<SIZE; row++) {
			boolean[] buffer = new boolean[SIZE];
			
			for(int column=0; column<SIZE; column++) {
				int value = getValueAt(column, row);
				if (value == 0) {
					continue;
				}
				if (buffer[value-1]) {
					return false;
				}
				buffer[value-1] = true;
			}
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(int r=0; r<SudokuGameboard.SIZE; r++) {
			for(int c=0; c<SudokuGameboard.SIZE; c++) {
				int value = getValueAt(c, r);
				sb.append(value == 0 ? " " : value);
				if ((c+1) % 3 == 0) sb.append(" ");
			}
			sb.append("\n");
			if ((r+1) % 3 == 0) sb.append("\n");
		}
		return sb.toString();
	}

	public boolean visit(GameboardVisitor visitor) {
		for(int r=0; r<SudokuGameboard.SIZE; r++) {
			for(int c=0; c<SimpleGameboard.SIZE; c++) {
				boolean visit;
				
				if (r % 3 == 0 && c % 3 == 0) {
					visit = visitor.visit(c, r);
					if (!visit) {
						return false;
					}
				}
				
				visit = visitor.visit(c, r, getValueAt(c, r));
				if (!visit) {
					return false;
				}
			}
		}
		return true;
	}
}
