package org.rydzewski.sudoku.impl;

import java.util.ArrayList;
import java.util.List;

import org.rydzewski.sudoku.GameboardGenerator;
import org.rydzewski.sudoku.SudokuGameboard;
import org.rydzewski.sudoku.WritableSudokuGameboard;

/**
 * Generates complete random board using brute force method
 * 
 * @author Mikolaj Rydzewski
 */
public class SimpleGenerator implements GameboardGenerator {

	public SudokuGameboard createGameboard() {
		PossibleBoard board = null;

		do {
			board = new PossibleBoard();
			for (int r = 0; r < SudokuGameboard.SIZE; r++) {
				for (int c = 0; c < SudokuGameboard.SIZE; c++) {
					board.randomizeField(c, r);
					board.fillObviousFields();
				}
			}
		} while (!board.isCorrect());

		return new SimpleGameboard().copyFrom(board);
	}

	/**
	 * Heavyweight gameboard for data generation
	 */
	private static class PossibleBoard extends AbstractSudokuGameboard implements WritableSudokuGameboard {

		private PossibleField[][] board = new PossibleField[SudokuGameboard.SIZE][SudokuGameboard.SIZE];

		PossibleBoard() {
			for (int c = 0; c < board.length; c++) {
				for (int r = 0; r < board[c].length; r++) {
					board[c][r] = new PossibleField(c, r);
				}
			}
		}

		/**
		 * Looks for non set fields, where only one possible value is allowed,
		 * then fills them
		 */
		void fillObviousFields() {
			boolean found;
			do {
				found = false;
				for (int r = 0; r < SudokuGameboard.SIZE; r++) {
					for (int c = 0; c < SudokuGameboard.SIZE; c++) {
						PossibleField field = board[c][r];
						if (field.isSet()) {
							continue;
						}
						List<Integer> allowedValues = field.getAllowedValues();
						if (allowedValues.size() == 1) {
							int value = allowedValues.get(0);
							try {
								setValueAt(c, r, value);
								found = true;
							} catch (IllegalStateException ignored) { // NOPMD
							}
						}
					}
				}
			} while (found);
		}

		/**
		 * Sets random value in specific field
		 * 
		 * @param column
		 * @param row
		 */
		void randomizeField(int column, int row) {
			PossibleField field = board[column][row];

			if (field.isSet()) {
				return;
			}

			List<Integer> allowedValues = field.getAllowedValues();
			if (allowedValues.isEmpty()) {
				return;
			}

			for (int i = allowedValues.size() - 1; i >= 0; i--) {
				Integer value = allowedValues.get(RandomUtils.nextInt(allowedValues.size()));
				if (willNotConflict(column, row, value)) {
					try {
						setValueAt(column, row, value);
						break;
					} catch (IllegalStateException ignored) { // NOPMD
					}
				}
				allowedValues.remove(value);
			}
		}

		/**
		 * Checks whether new value will not conflict with any values in
		 * crossing rows and column TODO add check for two axes
		 * 
		 * @param column
		 * @param row
		 * @param value
		 * @return
		 */
		boolean willNotConflict(int column, int row, int value) {
			List<PossibleField> vector = new ArrayList<SimpleGenerator.PossibleField>();

			// check only empty and not current fields
			for (int r = 0; r < SudokuGameboard.SIZE; r++) {
				PossibleField field = board[column][r];
				if (!field.isSet()) {
					vector.add(field);
				}
			}
			boolean inColumnOk = willVectorNotConflict(vector, value);

			vector.clear();
			for (int c = 0; c < SudokuGameboard.SIZE; c++) {
				PossibleField field = board[c][row];
				if (!field.isSet()) {
					vector.add(field);
				}
			}
			boolean inRowOk = willVectorNotConflict(vector, value);

			return inRowOk && inColumnOk;
		}

		/**
		 * Checks whether value will not fall in conflict with any of fields
		 * TODO how does it work???
		 * 
		 * @param fieldsToCheck
		 * @param value
		 * @return
		 */
		boolean willVectorNotConflict(List<PossibleField> fieldsToCheck, int value) {
			int[] possibilitesLeft = new int[fieldsToCheck.size()];

			for (int i = 0; i < fieldsToCheck.size(); i++) {
				PossibleField field = fieldsToCheck.get(i);
				List<Integer> allowedValues = field.getAllowedValues();
				possibilitesLeft[i] = field.isAllowed(value) ? allowedValues.size() - 1 : allowedValues.size();
			}
			int numSmaller = 0;
			for (int sizeLeft : possibilitesLeft) {
				if (sizeLeft < fieldsToCheck.size()) {
					numSmaller++;
				}
			}

			return numSmaller <= fieldsToCheck.size();
		}

		public int getValueAt(int column, int row) {
			return board[column][row].value;
		}

		/**
		 * Sets value at specific location and updates list of allowed fields in
		 * appropriate rows and columns
		 */
		public void setValueAt(int column, int row, int value) {
			board[column][row].setValue(value);

			for (int r = 0; r < SudokuGameboard.SIZE; r++) {
				board[column][r].removePossibleValue(value);
			}
			for (int c = 0; c < SudokuGameboard.SIZE; c++) {
				board[c][row].removePossibleValue(value);
			}
			int squareR = (row / 3) * 3;
			int squareC = (column / 3) * 3;
			for (int c = 0; c < 3; c++) {
				for (int r = 0; r < 3; r++) {
					board[squareC + c][squareR + r].removePossibleValue(value);
				}
			}
		}
	}

	/**
	 * Information about concrete value at location / possible values allowed
	 * there.
	 */
	private static class PossibleField {
		/**
		 * allowed values in that field. true means value is allowed
		 */
		private boolean[] values = new boolean[SudokuGameboard.SIZE];
		/**
		 * concrete value
		 */
		private int value;
		private final int col;
		private final int row;

		PossibleField(int col, int row) {
			this.col = col;
			this.row = row;
			for (int i = 0; i < SudokuGameboard.SIZE; i++) {
				values[i] = true;
			}
		}

		boolean isSet() {
			return value != 0;
		}

		void removePossibleValue(int value) {
			values[value - 1] = false;
			Validate.isTrue(!(this.value == 0 && getAllowedValues().isEmpty()), "No allowed values for empty field ");
		}

		void setValue(int value) {
			Validate.isTrue(value >= 1 && value <= 9, "Incorrect value " + value);
			Validate.isTrue(isAllowed(value), "Cannot set value " + value);
			this.value = value;
			values[value - 1] = false;
		}

		/**
		 * Checks whether value is allowed at this location
		 * 
		 * @param value
		 * @return
		 */
		boolean isAllowed(int value) {
			return values[value - 1];
		}

		List<Integer> getAllowedValues() {
			List<Integer> result = new ArrayList<Integer>();
			for (int i = 0; i < values.length; i++) {
				if (values[i]) {
					result.add(i + 1);
				}
			}
			return result;
		}

		@Override
		public String toString() {
			return String.format("%dx%d = %d, %s", col, row, value, getAllowedValues());
		}
	}
}
