package org.rydzewski.sudoku.impl;

public class Validate {

	public static void isTrue(boolean condition, String message) {
		if (!condition) {
			throw new IllegalStateException(message);
		}
	}

}
