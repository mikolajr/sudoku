package org.rydzewski.sudoku;

/**
 * Generic read only board. Board is modeled as two dimensional array of integer values.
 * @author Mikolaj Rydzewski
 */
public interface SudokuGameboard {
	
	final static int SIZE = 9;

	/**
	 * Returns value at specified location. Returns 0 if field is empty.
	 * @param column
	 * @param row
	 * @return
	 */
	int getValueAt(int column, int row);
	
	/**
	 * Verifies whether gameboard is correct
	 * @return
	 */
	boolean isCorrect();
	
	/**
	 * Verifies whether all fields are filled.
	 * @return
	 */
	boolean isCompleted();
	
	/**
	 * Visits every cell in the board.
	 * Returns return value from last call to {@link GameboardVisitor} visit method
	 * @param visitor
	 */
	boolean visit(GameboardVisitor visitor);
	
}
