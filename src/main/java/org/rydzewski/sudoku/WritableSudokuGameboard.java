package org.rydzewski.sudoku;

/**
 * Read write enhancement
 * @author Mikolaj Rydzewski
 */
public interface WritableSudokuGameboard extends SudokuGameboard {
	
	/**
	 * Sets value at the gameboard.
	 * @param column
	 * @param row
	 * @param value
	 */
	void setValueAt(int column, int row, int value);

}
