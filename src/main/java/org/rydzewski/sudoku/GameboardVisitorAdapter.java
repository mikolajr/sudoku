/**
 * 
 */
package org.rydzewski.sudoku;

/**
 * @author Mikolaj Rydzewski
 *
 */
public class GameboardVisitorAdapter implements GameboardVisitor {

	public boolean visit(int col, int row, int value) {
		return true;
	}

	public boolean visit(int col, int row) {
		return true;
	}

}
