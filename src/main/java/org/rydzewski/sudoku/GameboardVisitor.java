/**
 * 
 */
package org.rydzewski.sudoku;

/**
 * Callback interface for {@link SudokuGameboard#visit(GameboardVisitor)}
 * @author Mikolaj Rydzewski
 *
 */
public interface GameboardVisitor {
	
	/**
	 * Called for every visited cell in the gameboard
	 * @param col
	 * @param row
	 * @param value
	 * @return value <b>false</b> returned stops further visiting
	 */
	boolean visit(int col, int row, int value);
	
	/**
	 * Called for every visited super cell (3x3) in the gameboard
	 * @param col top-left column 
	 * @param row top-left row
	 * @return value <b>false</b> returned stops further visiting
	 */
	boolean visit(int col, int row);

}
